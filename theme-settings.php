<?php
/**
 * @file: Theme settings.
 */
function drzy_form_system_theme_settings_alter(&$form, $form_state) {
 // Breadcrumbs.
  $form['breadcrumb'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Breadcrumb settings'),
  );

  $form['breadcrumb']['drzy_breadcrumb'] = array(
    '#type'          => 'select',
    '#title'         => t('Display breadcrumb'),
    '#default_value' => theme_get_setting('drzy_breadcrumb'),
    '#options'       => array(
                          'yes'   => t('Yes'),
                          'admin' => t('Only in admin section'),
                          'no'    => t('No'),
                        ),
  );

  $form['breadcrumb']['breadcrumb_options'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        ':input[name="drzy_breadcrumb"]' => array('value' => 'no'),
      ),
    ),
  );

  $form['breadcrumb']['breadcrumb_options']['drzy_breadcrumb_separator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Breadcrumb separator'),
    '#description'   => t('Text only. Don’t forget to include spaces.'),
    '#default_value' => theme_get_setting('drzy_breadcrumb_separator'),
    '#size'          => 5,
    '#maxlength'     => 10,
  );

  $form['breadcrumb']['breadcrumb_options']['drzy_breadcrumb_home'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show home page link in breadcrumb'),
    '#default_value' => theme_get_setting('drzy_breadcrumb_home'),
  );

  $form['breadcrumb']['breadcrumb_options']['drzy_breadcrumb_trailing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append a separator to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('drzy_breadcrumb_trailing'),
    '#description'   => t('Useful when the breadcrumb is placed just before the title.'),
    '#states' => array(
      'disabled' => array(
        ':input[name="drzy_breadcrumb_title"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['breadcrumb']['breadcrumb_options']['drzy_breadcrumb_title'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append the content title to the end of the breadcrumb'),
    '#default_value' => theme_get_setting('drzy_breadcrumb_title'),
    '#description'   => t('Useful when the breadcrumb is not placed just before the title.'),
  );

  $form['support'] = array(
    '#type' => 'fieldset',
    '#title' => t('Accessibility and support settings'),
  );

  $form['support']['drzy_html5_respond_meta'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Add HTML5 and responsive scripts and meta tags to every page.'),
    '#default_value' => theme_get_setting('drzy_html5_respond_meta'),
    '#options' => array(
      'respond' => t('Add Respond.js JavaScript from Cloudfare CDN to add basic CSS3 media query support to IE 6-8.'),
      'html5' => t('Add HTML5 shim JavaScript from Google CDN to add support to IE 6-8 from'),
      'meta' => t('Add meta tags to support responsive design on mobile devices.'),
    ),
    '#description'   => t('IE 6-8 require a JavaScript polyfill solution to ' .
      'add basic support of HTML5 and CSS3 media queries. If you prefer to ' .
      'use another polyfill solution, such as <a href="!link">Modernizr</a>, ' .
      'you can disable these options. Respond.js only works if ' .
      '<a href="@url">Aggregate CSS</a> is enabled. Mobile devices require a ' .
      'few meta tags for responsive designs.',
      array(
        '!link' => 'http://www.modernizr.com/',
        '@url' => url('admin/config/development/performance'
      ))
    ),
  );
}

