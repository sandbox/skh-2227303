DRZY comes with a complete set of template files to override drupal core and
provide more semantic (and less!) markup.  Put your custom .tpl.php files in
here.  It is recommended that you copy them from the base theme's templates
folder.
