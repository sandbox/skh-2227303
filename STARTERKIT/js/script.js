/**
 * @file: script.js.
 */

(function ($) {

// Smooth scroll to hash links.
var smoothScroll = {
  init: function(context) {
    $('a[href*=#]:not([href=#])', context).click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  }
};

Drupal.behaviors.STARTERKIT = {
  // Run when DOM is loaded.
  attach: function (context, settings) {
    smoothScroll.init(context);
  }
};

})(jQuery);
