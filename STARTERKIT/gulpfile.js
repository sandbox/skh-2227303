// DRZY Requirements.
var gulp         = require('gulp'),
    // Utilities.
    gutil        = require('gulp-util'),
    plumber      = require('gulp-plumber'),
    changed      = require('gulp-changed'),
    // Styles.
    sass         = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    pixrem       = require('gulp-pixrem'),
    // Scripts.
    stylish      = require('jshint-stylish'),
    jshint       = require('gulp-jshint'),
    // Images.
    imagemin     = require('gulp-imagemin'),
    // Servers.
    livereload   = require('gulp-livereload'),
    lr           = require('tiny-lr'),
    server       = lr(),
    // Flags etc.
    errorPlugins = new Array(),
    hasBeeped    = false,
    hasReloaded  = false;

// Paths.
var paths = {
  src: {
    sass: './sass/*.scss',
    css:  './css/**/*.css',
    js:   './js/*.js',
    img:  './img/src/**/*'
  },
  dest: {
    sass: './sass',
    css:  './css',
    img:  './img/build'
  }
};

// Save console log function.
var cl = console.log;

// Override console log function.
console.log = function () {

  // Get the message being logged.
  var args = Array.prototype.slice.call(arguments);

  // Check that the is a message.
  if (args.length) {

    // Supress all the "... Reload [file] ..." messages except the first.
    if (/^\.\.\. Reload.*$/.test(args[0])){

      // If not first message, supress message.
      if (hasReloaded === true) {
        return;
      }

      // Save that message has shown.
      hasReloaded = true;

      // Update the message to something simpler.
      args[0] = "Reloading assets";
      args[1] = "";
    }
  }

  // Not a blocked message, show it.
  return cl.apply(console, args);
};

// Custom error handler.
var errorHandler = function(err) {

  // See if this plugin has set an error yet.
  if (errorPlugins.indexOf(err.plugin) < 0) {

    // Add plugin to used list so as to not show a too many messages.
    errorPlugins.push(err.plugin);

    // Show all characters (eg \n instead of a newline).
    err.message = JSON.stringify(err.message);

    // Log the error.
    gutil.log(

      // Show the plugin name in red.
      gutil.colors.red('[' + err.plugin + ']'),

      // Show the message.
      err.plugin === 'gulp-sass'?
        err.message.substr(err.message.lastIndexOf("/") + 1) : err.message
    );

    gutil.beep();

    // OneBeep.
    if (!hasBeeped) {

      hasBeeped = true;
    }
  }
};

// Livereload server setup.
gulp.task('lr-server', function() {
  server.listen(35729, function(err) {
    if(err) return console.log(err);
  });
});

// Development style tasks.
gulp.task('styles', function() {
  gulp.src([paths.src.sass])
    .pipe(plumber({ errorHandler: errorHandler }))
    .pipe(sass({
      sourceComments: 'map',
      includePaths : [paths.dest.sass]
    }))
    .pipe(autoprefixer('last 2 version', 'ie 8', 'ie 9'))
    .pipe(gulp.dest(paths.dest.css))
    .pipe(livereload(server));
});

// Production style tasks.
gulp.task('styles-prod', function() {
  gulp.src([paths.src.sass])
    .pipe(plumber({ errorHandler: errorHandler }))
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(autoprefixer('last 2 version', 'ie 8', 'ie 9'))
    .pipe(pixrem('62.5%'))
    .pipe(gulp.dest(paths.dest.css))
});

// Development script tasks.
gulp.task('scripts', function() {
  gulp.src([paths.src.js])
    .pipe(plumber({ errorHandler: errorHandler }))
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
    .pipe(livereload(server));
});

// Production script tasks.
gulp.task('scripts-prod', function() {
  gulp.src([paths.src.js])
    .pipe(plumber({ errorHandler: errorHandler }))
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});

// Development image tasks.
gulp.task('images', function() {
  gulp.src([paths.src.img])
    .pipe(plumber({ errorHandler: errorHandler }))
    .pipe(changed(paths.dest.img))
    .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    .pipe(gulp.dest(paths.dest.img))
    .pipe(livereload(server));
});

// Production image tasks.
gulp.task('images-prod', function() {
  gulp.src([paths.src.img])
    .pipe(plumber({ errorHandler: errorHandler }))
    .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    .pipe(gulp.dest(paths.dest.img));
});

// For production.
gulp.task('production', ['styles-prod', 'scripts-prod', 'images-prod']);

// For development (default task(.
gulp.task('default', ['lr-server', 'styles', 'scripts', 'images'], function() {

  // Watch styles.
  gulp.watch(paths.src.sass, ['styles']);

  // Watch scripts.
  gulp.watch(paths.src.js, ['scripts']);

  // Watch images.
  gulp.watch(paths.src.img, ['images']);
});

