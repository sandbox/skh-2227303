Quick Install:

1. $ drush dl <DRZY>
   $ cp -R sites/all/themes/drzy/STARTERKIT sites/default/themes/MYTHEME
   $ cd sites/default/themes/MYTHEME
   $ mv STARTERKIT.info.txt MYTHEME.info
   $ vi MYTHEME.info
   $ vi template.php
        :$s/STARTERKIT/MYTHEME/g
2. $ npm install; gulp
3. Enable theme.
4. Download livereload: http://bit.ly/1ka3ni3

Requirement: node.js http://nodejs.org
