<?php

/**
 * @file template.php
 */

/**
 * Implementation of template_preprocess_html().
 */
/* Delete this line to enable the function.
function STARTERKIT_preprocess_html(&$variables) {

}
// */

/**
 * Implementation of template_preprocess_page().
 */
/* Delete this line to enable the function.
function STARTERKIT_preprocess_page(&$variables) {
  // Hide feed icon.
  $variables['feed_icons'] = '';
}
// */

/**
 * Implementation of template_preprocess_node().
 */
/* Delete this line to enable the function.
function STARTERKIT_preprocess_node(&$variables) {

}
// */

/**
 * Implementation of template_preprocess_region().
 */
/* Delete this line to enable the function.
function STARTERKIT_preprocess_region(&$variables) {

}
// */

/**
 * Implementation of template_preprocess_field().
 */
/* Delete this line to enable the function.
function STARTERKIT_preprocess_field(&$variables) {

}
// */
