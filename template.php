<?php

/**
 * @file template.php
 */

/**
 * Implementation of template_preprocess_html().
 */
function drzy_preprocess_html(&$variables) {
  // Get settings for HTML5 and responsive support. array_filter() removes
  // items from the array that have been disabled.
  $html5_respond_meta = array_filter(
    (array) theme_get_setting('drzy_html5_respond_meta')
  );
  $variables['add_respond_js'] = in_array('respond', $html5_respond_meta);
  $variables['add_html5_shim'] = in_array('html5', $html5_respond_meta);
  $variables['default_mobile_metatags'] = in_array('meta', $html5_respond_meta);
}

/**
 * Implementation of template_preprocess_region().
 */
function drzy_preprocess_region(&$variables) {
  $region = $variables['region'];

  // Allow sidebars to share theme functions.
  if ($region == 'sidebar_first' || $region == 'sidebar_second') {
    array_unshift($variables['theme_hook_suggestions'], 'region__sidebar');
    $variables['classes_array'][] = 'sidebar';
  }

  // Inner wrapper attributes.
  $variables['inner_attributes'] = array(
    'class' => array('region-inner'),
  );
}

/**
 * Implementation of template_preprocess_block().
 */
function drzy_preprocess_block(&$variables) {

  // Add nicer classes to block content wrappers.
  $variables['content_attributes_array']['class'] = array(
    $variables['block_html_id'] . '-content',
    'content',
    'block-content',
  );
}

/**
 * Implementation of theme_field().
 *
 * Set cleaner markup for fields.
 */
function drzy_field($variables) {
  $output = array(
    'title' => array(),
    'content' => array(),
  );

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output['title'] = array(
      '#prefix' => '<div class="field-label"' . $variables['title_attributes'] . '>',
      '#markup' => $variables['label'] . ':&nbsp;',
      '#suffix' => '</div>',
    );
  }

  if (count($variables['items']) < 1) {
    return render($output);
  }

  if (count($variables['items']) > 1) {
    $output['content'] = array(
      '#prefix' => '<div class="field-items"' . $variables['content_attributes'] . '>',
      '#suffix' => '</div>',
    );

    // Render the items.
    foreach ($variables['items'] as $delta => $item) {
      $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');

      $output['content'][] = array(
        '#prefix' => '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>',
        '#suffix' => '</div>',
        'item' => $item,
      );
    }
  }
  else {
    $variables['classes'] .= " field-item odd";
    $output['content'][] = $variables['items'][0];
  }

  $output += array(
    '#prefix' => '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>',
    '#suffix' => '</div>',
  );

  // Render the top-level DIV.
  return render($output);
}


/**
 * Return a themed breadcrumb trail.
 *
 * @param $variables
 *   - title: An optional string to be used as a navigational heading to give
 *     context for breadcrumb links to screen-reader users.
 *   - title_attributes_array: Array of HTML attributes for the title. It is
 *     flattened into a string within the theme function.
 *   - breadcrumb: An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function drzy_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $output = '';

  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('drzy_breadcrumb');
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('drzy_breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = filter_xss_admin(theme_get_setting('drzy_breadcrumb_separator'));
      $trailing_separator = $title = '';
      if (theme_get_setting('drzy_breadcrumb_title')) {
        $item = menu_get_item();
        if (!empty($item['tab_parent'])) {
          // If we are on a non-default tab, use the tab's title.
          $breadcrumb[] = check_plain($item['title']);
        }
        else {
          $breadcrumb[] = drupal_get_title();
        }
      }
      elseif (theme_get_setting('drzy_breadcrumb_trailing')) {
        $trailing_separator = $breadcrumb_separator;
      }

      // Provide a navigational heading to give context for breadcrumb links to
      // screen-reader users.
      if (empty($variables['title'])) {
        $variables['title'] = t('You are here');
      }
      // Unless overridden by a preprocess function, make the heading invisible.
      if (!isset($variables['title_attributes_array']['class'])) {
        $variables['title_attributes_array']['class'][] = 'element-invisible';
      }

      // Build the breadcrumb trail.
      $output = '<nav class="breadcrumb" role="navigation">';
      $output .= '<h2' . drupal_attributes($variables['title_attributes_array']) . '>' . $variables['title'] . '</h2>';
      $output .= '<ol><li>' . implode($breadcrumb_separator . '</li><li>', $breadcrumb) . $trailing_separator . '</li></ol>';
      $output .= '</nav>';
    }
  }

  return $output;
}

/**
 * Implementation of template_preprocess_node().
 */
function drzy_preprocess_node(&$variables) {
  // Add a view-mode specific pre process function.
  $func = $GLOBALS['theme'] . '_preprocess_node__' . $variables['view_mode'];

  // Call the function if it exists.
  if (function_exists($func)) {
    $func($variables);
  }

  // Add a view-mode specific theme suggestion.
  $variables['theme_hook_suggestions'][] =
    'node__view_mode__' . $variables['view_mode'];
}
